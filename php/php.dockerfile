FROM php:8.0-fpm

RUN apt-get update && \
    apt-get install -y \
      git

# Install Zip
RUN apt-get install -y \
      libzip-dev \
      zip \
      unzip \
    && docker-php-ext-install zip

RUN apt-get update -y \
    && apt-get install -y \
      libpng-dev \
      libjpeg-dev \
      libxpm-dev \
      libfreetype6-dev \
      libvpx-dev \
    && docker-php-ext-configure gd \
    --with-freetype \
    --with-jpeg \
    --with-xpm 

RUN docker-php-ext-install gd
RUN docker-php-ext-install exif

# Install Postgres PDO
# RUN apt-get update && \
    # apt-get install -y postgresql-client libpq-dev && \
    # docker-php-ext-install pdo pdo_pgsql pgsql

# Install and enable xDebug
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

RUN curl --silent --show-error https://getcomposer.org/installer | php && \
   mv composer.phar /usr/local/bin/composer


RUN apt-get update \
  && apt-get install -y --no-install-recommends libpq-dev \
  && docker-php-ext-install mysqli pdo_pgsql pdo_mysql